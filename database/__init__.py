import os
import subprocess
import MySQLdb as mdb
import wave
import pyaudio


class DatabaseConnection:

    def __init__(self) -> None:
        """

        """

        self.con = 0

    def Connect(self, host: str, username: str, password: str, database: str) -> None:
        """
        :param host:
        :param username:
        :param password:
        :param database:
        :return:
        """
        try:
            self.con = mdb.connect(host, username, password, database)
        except mdb.Error:
            print("Error connecting to database " , str)

    def Close(self) -> None:
        """
        :return:
        """
        if self.con:
            self.con.close()

    def StoreFileLocations(self, dir: str) -> None:
        """

        :param dir:
        :return:
        """

        query = subprocess.Popen(["ls", "*.wav"], stdout=subprocess.PIPE)
        output, err = query.communicate()

        fileList = output.split('\n');
        cur = self.con.cursor()

        for filename in fileList:
            if filename:
                fullpath = os.path.dirname(os.path.realpath(filename))
                # mysql can't handle \ so lets convert to /
                fullpath = '/'.join(fullpath.split('\\'))
                # determine if the filename is already in the db
                query = 'SELECT * FROM fileExtensions WHERE filename = \'' + filename + '\'';
                cur.execute(query)
                if cur.rowcount == 0:
                    # no filename in the db so lets go ahead and add one
                    query = 'INSERT INTO fileExtensions VALUES('+'\''+filename+'\',\''+fullpath+'\')'
                else:
                    # if the filename is in the db then update the path just in case
                    query = 'UPDATE fileExtensions SET file = \''+fullpath+'\' WHERE filename = \''+filename+'\''
                cur.execute(query)
                self.con.commit()

    def PullWavData(self) -> [str]:
        """

        :return:
        """

        cur = self.con.cursor()
        cur.execute("SELECT * FROM fileExtensions")

        rows = cur.fetchall()

        for row in rows:
            # let's put together the string for the whole filename
            audio_file = row[1] + '/' + row[0]
            wf = wave.open(audio_file, 'rb')
            # play that audio file (for now).
            # go through and play it byte by byte
            p = pyaudio.PyAudio()

            stream = p.open(
                format=p.get_format_from_width(wf.getsampwidth()),
                channels=wf.getnchannels(),
                rate=wf.getframerate(),
                output=True)
            data = wf.readframes(1024)
            while data != '':
                stream.write(data)
                data = wf.readframes(1024)

            stream.close()
            p.terminate()

    def Export(self, filename: str) -> None:
        """
        Create a script that will be used to insert all data for later dates.
        :return:
        """

        with open(filename, "a") as myfile:
            myfile.write('DROP DATABASE IF EXISTS voiceRecognition;\n')
            myfile.write('CREATE DATABASE voiceRecognition;\n')
            myfile.write('USE voiceRecognition;\n')

            cur = self.con.cursor()

            cur.execute('SHOW TABLES');
            rows = cur.fetchall()

            for row in rows:
                myfile.write('\nCREATE TABLE IF NOT EXISTS ' + row[0] + '\n(\n')

                query = 'Select COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = \'' + row[0] + '\''
                cur.execute(query)

                schema = cur.fetchall()

                for entry in schema:
                    # varchar is a special case -> make an exception for it.
                    line = entry[0] + ' VARCHAR(200)' if entry[1] == 'varchar' else entry[0] + ' ' + entry[1]

                    # if not last element, add a comma separator
                    if entry is not schema[-1]:
                        line += ','

                    # Write each line to the file
                    myfile.write('\t' + line + '\n')

                myfile.write(') ENGINE = innodb;\n\n')

                # Now we have all of the tables - get all of the elements
                query = 'Select * FROM ' + row[0]
                cur.execute(query)
                selected = cur.fetchall()

                for srow in selected:
                    myfile.write('INSERT INTO ' + row[0] + ' VALUES(')
                    for item in srow:
                        if item is not srow[-1]:
                            myfile.write('\'' + item + '\',')
                        else:
                            myfile.write('\'' + item + '\');\n')

        myfile.close()
