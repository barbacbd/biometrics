import math


def rms(x: [float]) -> float:
    """
    :param x:
    :return: Root Mean Squared of the list
    """
    tlist = [i ** 2 for i in x]
    return math.sqrt(mean(tlist))


def mean(x: [float]):
    """
    :param x: list of floats
    :return: the mean value of the list
    """
    total = 0.0
    for i in range(0, len(x)):
        total = total + x[i]
    return total / float(len(x))


def comparison(a, b, less=True, equal=False):
    """
    compare all of the elements in a to the list, float, or int b. If they
    are lists, then the size of the lists should be equal.
    :param a: list of input
    :param b: list, float, int to compare elements against
    :param less: default to true, if it is less than or greater than
    :param equal: default to false, if it is >= or <=
    :return: a list of 0s and 1s. The 1s mean that the value of the criteria was met, 0 otherwise
    """
    aPoints = len(a)
    result = [0] * aPoints
    if isinstance(a, list) and isinstance(b, list):
        bPoints = len(b)
        if aPoints != bPoints:
            return
        for i in range(0, aPoints):
            if less:
                if equal:
                    if a[i] <= b[i]:
                        result[i] = 1
                else:
                    if a[i] < b[i]:
                        result[i] = 1
            else:
                if equal:
                    if a[i] >= b[i]:
                        result[i] = 1
                else:
                    if a[i] > b[i]:
                        result[i] = 1
        return result
    elif isinstance(a, list) and \
        (isinstance(b, int) or isinstance(b, float)):
        for i in range(0, aPoints):
            if less:
                if equal:
                    if a[i] <= b:
                        result[i] = 1
                else:
                    if a[i] < b:
                        result[i] = 1
            else:
                if equal:
                    if a[i] >= b:
                        result[i] = 1
                else:
                    if a[i] > b:
                        result[i] = 1
        return result
    else:
        return result


def findpos(x):
    """
    Find the positions where there was a value of one in the array
    :param x:
    :return: array position + 1
    """
    nPoints = len(x)
    result = []
    for i in range(0, nPoints):
        if x[i] == 1:
            result = result + [i]
    return result


def mergesort(matrix):
    """
    Run the merge sort algorithm on a list
    :param matrix:
    :return:
    """
    if len(matrix) > 1:
        mid = len(matrix)//2
        lefthalf = matrix[:mid]
        righthalf = matrix[mid:]

        mergesort(lefthalf)
        mergesort(righthalf)

        i = 0
        j = 0
        k = 0

        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                matrix[k] = lefthalf[i]
                i = i+1
            else:
                matrix[k] = righthalf[j]
                j = j +1
            k=k+1
        while i < len(lefthalf):
            matrix[k] = lefthalf[i]
            i = i+1
            k=k+1
        while j< len(righthalf):
            matrix[k]=righthalf[j]
            j = j+1
            k=k+1


def find_median(x):
    """
    Find the median value of the array
    :param x:
    :return:
    """
    return float(x[int(len(x)/2)-1] + x[int(len(x)/2)])/2 if (len(x)%2==0) else float(x[int(len(x)/2)])


def valuelist(start, step, end, div=1):
    """

    :param start:
    :param step:
    :param end:
    :param div:
    :return:
    """
    temp =  []
    while start < end:
        temp.append(start/div)
        start = start + step
    return temp


def maxlist(x):
    """
    Max list finds the maximum value in the list
    :param x:
    :return:
    """
    maxval = -float('inf')
    for i in x:
        if i > maxval:
            maxval = i
    return maxval


def step_list(a,b,step):
    """
    Make a list from a to b with a step of size step
    :param a:
    :param b:
    :param step:
    :return:
    """
    temp = [0.0] * int(((b-a)/step)+1)
    for i in range(0, len(temp)):
        temp[i] = a
        a = a + step
    return temp


def make_list(a, b, c):
    """
    Make a list out of the values and list variables
    :param a:
    :param b:
    :param c:
    :return:
    """
    temp = [0] * (len(a) + len(b) + len(c))
    for i in range(0, len(temp)):
        if i >= (len(a)+len(b)):
            temp[i] = c[i-(len(a)+len(b))]
        elif i >= len(a) and i < (len(b)+len(a)):
            temp[i] = b[i-len(a)]
        else:
            temp[i] = a[i]
    return temp


def mtrx_mlt(a, b):
    """
    Multiply the two 1D matrices
    :param a:
    :param b:
    :return:
    """
    if len(a) != len(b):
        return
    temp = 0
    for i in range(0, len(a)):
        temp = temp + (a[i] * b[i])
    return temp


def normalize(x):
    """
    Normalize the list
    :param x:
    :return:
    """
    maxval = -float('inf')
    temp = [0.0] * len(x)
    for i in range(0, len(x)):
        if abs(x[i]) > maxval:
            maxval = abs(x[i])
    for i in range(0, len(x)):
        temp[i] = float(x[i])/float(maxval)
    return temp


def dc_removal(x):
    """

    :param x:
    :return:
    """
    total = 0.0
    temp = [0.0]*len(x)
    for i in range(0, len(x)):
        total = total + float(x[i])
    total = total / float(len(x))
    for i in range(0, len(x)):
        temp[i] = float(x[i])-total
    return temp


def avg_list(a, b):
    """

    :param a:
    :param b:
    :return:
    """
    if len(a) != len(b):
        return
    for i in range(0, len(a)):
        a[i] = (a[i]+b[i])/2.
    return a


def median_smooth(x, L):
    """
        Median Smoothing filter.
        The order of the filter has very different effects on the
        output. Even order tends to have interpolation effects,
        whcih is simular to smoothing.
    """
    y = [0] * len(x)  # zero fill the array
    if L%2 == 0:
        l = L/2
        x1 = make_list([x[0]]*(l-1), x, [x[len(x)-1]]*(l+1))
        x2 = make_list([x[0]]*(l), x, [x[len(x)-1]]*l)
    else:
        l =(L-1)/2
        x1 = make_list([x[0]]*l, x, [x[len(x)-1]]*(l+1))
    for i in range(0, len(x)):
        temp = x1[i:i+L]
        mergesort(temp)
        y[i] = find_median(temp)
    if L%2 == 0:
        y1 = [0] * len(x)
        for i in range(0, len(x)):
            temp = x2[i:i+L]
            mergesort(temp)
            y1[i] = find_median(temp)
        y = avg_list(y, y1)
    return y


def sum_odd_even(x, odd=True):
    """
    Sum the odd or even rows for each column. This is odd, because
    we must think of these in terms of starting the number at index = 1
    where index 0 =1 in this case
    :param x:
    :param odd:
    :return:
    """
    temp = []
    for j in range(0, len(x[0])): # for each column
        total = 0.0
        for i in range(0, len(x)): # for each row
            if odd and i % 2 == 0: # 0 is odd (acting like 1)
                total = total + x[i][j]
            elif not odd and i%2==1:
                total = total + x[i][j]
        temp.append(total)
    return temp


def vda(x,segmentdur,noisefloor=0.01,minzcr=3000):
    """
    voice = vda(x) determine whether the segment is voiced, unvoiced, or silent
    this VDA is independent from PDA process. It does NOT take advantage of info
    derived from PDA, this requires more computation.
    :param x:
    :param segmentdur:
    :param noisefloor:
    :param minzcr:
    :return:
    """
    row = len(x)
    col = len(x[0])
    voice = [1] * row
    engergy = [0.0] * row
    for i in range(0, row):
        total = 0.0
        for j in range(0, col):
            total = total + x[i][j]**2.
        engergy[i] = total
    index = findpos(comparison(engergy,(noisefloor*3),True, True))
    for i in index:
        voice[i] = 0
    return voice


def twomax(x, lower, upper, unitlen):
    """
    Find the two maxima peaks
    :param x:
    :param lower:
    :param upper:
    :param unitlen:
    :return:
    """
    lenx=len(x)
    halfoct=round(1./unitlen/2.)
    maxval = -float("inf")
    j = lower-1
    index = 0
    for i in x[lower-1:upper]:
        if i > maxval:
            maxval = i
            index = j
        j = j + 1

    indices = []
    maxes = []
    maxes.append(maxval)
    indices.append(index)
    harmonics = 2.
    LIMIT=0.0625 # 1/8 octave
    startpos = index+round(math.log(harmonics - LIMIT, 2) / (unitlen * 1.0))
    if startpos <= min(lenx,upper):
        endpos = index + int(round(math.log(harmonics + LIMIT, 2) / (unitlen * 1.0)))
        if endpos > min(lenx, upper):
            endpos = min(lenx,upper)
        maxval1 = -float("inf")
        j = int(startpos)
        index1 = 0
        for i in x[int(startpos):int(endpos)+1]:
            if i > maxval1:
                maxval1 = i
                index1 = j
            j = j + 1
        if maxval1 > 0:
            maxes.append(maxval1)
            indices.append(index1)
    return maxes, indices


def cut_list(x, start, end):
    """
    Cut down the size of the list
    :param x:
    :param start:
    :param end:
    :return:
    """
    temp = [[0.0] * (end-start+1)] * len(x)
    for i in range(0, len(x)):
        temp[i] = x[i][start:end]
    return temp
