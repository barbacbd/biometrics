import numpy as np
import math
import Pitch.pitch_math as pm
from Pitch.pitch import Pitch
import pyaudio
import wave
import struct


def shrp(y, fs, f0minmax=[50, 500], framelength=40, timestep=10, threshold=0.4, ceiling=1250,
         med_smooth=0, check_voicing=0):
    """
    Pitch determination algorithm based on Subharmonic to
    Harmonic ration (SHR)

    Input parameters:
        y:              input data
        fs:             sampling frequency
        f0minmax:       2d array specifies the f0 range [minf0, maxf0]
                            -> default [50, 550]
        framelength:    length of each frame in milliseconds -> default 40 ms
        timestep:       interval for updating short-term analysis in millisecond
                            -> default 10
        threshold:      subharmonic to harmonic ratio threshold in the range of
                            [0,1] -> default 0.4
        ceil:           upper bound of the frequencies that are used for estimating
                            pitch -> default 1250 hz
        smooth:         order of the median smoothing -> default 0, no smoothing
        check_voicing:  0 no voice checking (default), 1 voice checking

    Output parameters:
        f0_time:        an array stores the times for the F0 points
        f0_value:       an array stores F0 values
        SHR:            an array stores subharmonic-to-harmonic ratio for each frame
        f0_candidates:  a matrix stores the f0 candidates for each frames

        Permission to use, copy, modify, and distribute this software without fee
        is hereby granted
        FOR RESEARCH PURPOSES only, provided that this copyright notice appears
        in all copiesand in all supporting documentation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

        For details of the algorithm, please see
        Sun, X.,"Pitch determination and voice quality analysis using
        subharmonic-to-harmonic ratio" To appear in the Proc. of ICASSP2002,
        Orlando, Florida, May 13 -17, 2002.
        For update information, check http://mel.speech.nwu.edu/sunxj/pda.htm

        Copyright (c) 2001 Xuejing Sun
        Department of Communication Sciences and Disorders
        Northwestern University, USA
        sunxj@northwestern.edu

        Update history:
            Added "f0_candidates" as a return value, Dec. 21, 2001
            Changed default median smoothing order from 5 to 0, Jan. 9, 2002
        Modified the GetLogSpectrum function, bug fixed due to Herbert Griebel.
        Jan. 15, 2002
        Several minor changes. Jan. 15,2002.

        *** Original development completed in matlab, this code has been viewed
        and used as a source for python development ***
    """
    minf0 = f0minmax[0]
    maxf0 = f0minmax[1]
    duration = framelength

    # pre processing input signal
    y = pm.dc_removal(y)
    y = pm.normalize(y)  # normalization
    total_len = len(y)

    # specify some algorithm specific thresholds
    interp_depth = 0.5  # for fft length

    # derived thresholds specific to the algorithm
    maxlogf = math.log((maxf0 / 2.), 2)
    minlogf = math.log((minf0 / 2.), 2)
    N = (math.floor(ceiling / minf0) - math.floor(ceiling / minf0) % 2) * 4

    # derive number of frames necessary to use
    segmentlen = round(duration * (float(fs) / 1000.))
    inc = round(float(timestep) * (float(fs) / float(1000)))
    nf = int(math.floor((total_len - segmentlen + inc) / inc))
    n = [i for i in range(1, nf + 1)]
    f0_time = [(i - 1) * float(timestep) + (duration/2) for i in n]

    # determine fft length
    fftlen = 1
    while fftlen < segmentlen * (1 + interp_depth):
        fftlen = fftlen * 2

    # derive linear and og frequency scale
    frq = [(fs * (i + 1) / float(fftlen)) for i in range(0, fftlen / 2)]
    limit = pm.findpos(pm.comparison(frq, ceiling, False, True))
    lim = limit[0]  # only the first is useful
    frq = frq[0:lim + 1]
    logf = [i + 4 for i in frq]

    # the minimum distance between two points after interpolation
    minbin = logf[len(logf) - 1] - logf[len(logf) - 2]

    shift = [i + 4 for i in n]
    shift_units = [0] * len(shift)  # shift distance
    for i in range(0, len(shift)):
        shift_units[i] = round(shift[i] / minbin)
    shift = shift[len(shift) - 2]
    shift_units = shift_units[len(shift_units) - 2]
    j = [i for i in range(2, int(N + 1))]

    # the following ar universal for all of the frames
    #
    # find out all the start position of each shift
    temp = [(i + 4) / minbin for i in j]

    startpos = [0] * len(temp)
    for i in range(0, len(temp)):
        startpos[i] = round(shift_units + 1 - temp[i])
    index = pm.findpos(pm.comparison(startpos, 1))
    for i in index:
        startpos[i] = 1
    interp_logf = pm.step_list(logf[0], logf[len(logf) - 1], minbin)
    interp_len = len(interp_logf)
    totallen = shift_units + interp_len
    endpos = [i + (interp_len - 1) for i in startpos]
    index = pm.findpos(pm.comparison(endpos, totallen, False))
    for i in index:
        endpos[i] = totallen

    # the linear Hz scale derived from the interpolated log scale
    newfre = [2**i for i in interp_logf]
    # find out the index of upper bound of search region
    # on the log frequency scale.
    upper = pm.findpos(pm.comparison(interp_logf, maxlogf, False, True))
    upper = upper[0]  # only the first is useful
    lower = pm.findpos(pm.comparison(interp_logf, minlogf, False, True))
    lower = lower[0]

    # segmentation of speech
    curpos = [round((i / 1000) * fs) for i in f0_time]

    frames = to_frames(y, curpos, segmentlen, "hamm")

    nf = len(frames)

    # initialize vectors for f0 time, f0 values, and shr
    f0_value = [0] * nf
    shr = [0] * nf
    f0_time = f0_time[0:nf]
    f0_candidates = [[0] * nf, [0] * nf]

    # voicing determination
    if check_voicing:
        NoiseFloor = sum([i**2 for i in frames[0]])
        voicing = pm.vda(frames, duration / 1000, NoiseFloor)
    else:
        voicing = [1] * nf

    # the main loop
    curf0 = 0
    cur_shr = 0
    cur_cand1 = 0
    cur_cand2 = 0
    for i in range(0, nf):
        segment = frames[i]
        curtime = f0_time[i]
        if voicing[i] == 0:
            curf0 = 0
            cur_shr = 0
        else:
            log_spectrum = get_log_spectrum(segment, fftlen, lim, logf, interp_logf)
            (peak_index, cur_shr, shshift, all_peak_indices) = \
                compute_shr(log_spectrum, minbin, startpos, endpos, lower, upper, N, shift_units, threshold)
            # print all_peak_indices
            if peak_index == -1:
                if check_voicing:
                    curf0 = 0
                    cur_cand1 = 0
                    cur_cand2 = 0
            else:
                curf0 = newfre[peak_index] * 2
                if curf0 > maxf0:
                    curf0 = curf0 / 2.
                if len(all_peak_indices) == 1:
                    cur_cand1 = 0
                    cur_cand2 = newfre[all_peak_indices[0]] * 2
                else:
                    cur_cand1 = newfre[all_peak_indices[0]] * 2
                    cur_cand2 = newfre[all_peak_indices[1]] * 2
                if cur_cand1 > maxf0:
                    cur_cand1 = cur_cand1 / 2.
                if cur_cand2 > maxf0:
                    cur_cand2 = cur_cand2 / 2.
                if check_voicing:
                    voicing[i] = pm.postvda(segment, curf0, fs)
                    if voicing[i] == 0:
                        curf0 = 0
        f0_value[i] = curf0
        shr[i] = cur_shr
        # these are a little backwards because we are
        # representing a column array using only a row array
        f0_candidates[0][i] = cur_cand1
        f0_candidates[1][i] = cur_cand2

    # post processing
    if med_smooth > 0:
        f0_value = pm.medsmooth(f0_value, med_smooth)

    return f0_time, f0_value, shr, f0_candidates


def compute_pitch(filename: str):
    """
    Input a filename and determine the musical note for each chunk of data
    :param filename:
    :return:
    """
    # Adjusting up/down is more/less accurate
    # (larger mean less points) - multiples of 2
    chunk = 4096 * 2
    wf = wave.open(filename, 'rb')  # open up a wave
    swidth = wf.getsampwidth()
    RATE = wf.getframerate()
    window = np.blackman(chunk)  # use a Blackman window
    p = pyaudio.PyAudio()  # open stream
    stream = p.open(format=
                    p.get_format_from_width(wf.getsampwidth()),
                    channels=wf.getnchannels(),
                    rate=RATE,
                    output=True)

    data = wf.readframes(chunk)  # read some data
    thefreq = 0
    # larger chunks
    # chunk = len(data)/2 if (len(data)%2==0) else (len(data)-1)/2

    # play stream and find the frequency of each chunk
    while len(data) == chunk * swidth:
        stream.write(data)  # write data out to the audio stream
        # unpack the data and times by the hamming window
        indata = np.array(wave.struct.unpack("%dh" % (len(data) / swidth), \
                                             data)) * window
        fftData = abs(np.fft.rfft(indata)) ** 2  # Take the fft and square each value
        which = fftData[1:].argmax() + 1  # find the maximum
        if which != len(fftData) - 1:  # use quadratic interpolation around the max
            y0, y1, y2 = np.log(fftData[which - 1:which + 2:])
            x1 = (y2 - y0) * .5 / (2 * y1 - y2 - y0)
            thefreq = (which + x1) * RATE / chunk  # find the frequency and output it
        else:
            thefreq = which * RATE / chunk  # find the frequency and output it
        note, col = get_note(thefreq)  # get the pitch for the current frequency
        data = wf.readframes(chunk)  # read some more data
    if data:
        stream.write(data)
    stream.close()
    p.terminate()


def compute_shr(log_spectrum, min_bin, startpos, endpos, lower, upper, N, shift_units, shr_threshold):
    """
    Compute the shr values and find all of the candidate fundamental frequencies (pitches)
    :param log_spectrum:
    :param min_bin:
    :param startpos:
    :param endpos:
    :param lower:
    :param upper:
    :param N:
    :param shift_units:
    :param shr_threshold:
    :return:
    """
    len_spectrum = len(log_spectrum)
    totallen = int(shift_units) + int(len_spectrum)
    shshift = {}
    for i in range(0, int(N)):
        shshift[i] = []  # create a new list each iteration
        if i == 0:
            for j in range(0, totallen):
                if j >= (totallen - len_spectrum):
                    shshift[i].append(log_spectrum[j - (totallen - len_spectrum)])
                else:
                    shshift[i].append(0.0)
        else:
            shshift[i] = [0] * int(startpos[i - 1] - 1) + log_spectrum + [0] * int(totallen - endpos[i - 1])

    shshift = pm.cut_list(shshift, int(shift_units), len(shshift[0]))
    shodd = pm.sum_odd_even(shshift)
    sheven = pm.sum_odd_even(shshift, False)

    difference = []
    for i in range(0, len(sheven)):
        difference.append(sheven[i] - shodd[i])

    # peak picking process
    shr = 0
    (mag, index) = pm.twomax(difference, lower, upper, min_bin)

    if len(mag) == 1:
        peak_index = -1 if mag[0] <= 0 else index[0]
    else:
        shr = (mag[0] - mag[1]) / (mag[0] + mag[1])
        peak_index = index[1] if shr <= shr_threshold else index[0]

    return peak_index, shr, shshift, index


def to_frames(x, curpos, segmentlen, wintype):
    """
    Split the signal into frames
    :param x:
    :param curpos:
    :param segmentlen:
    :param wintype:
    :return:
    """
    lenx = len(x)
    numFrames = len(curpos)
    start = [i - round(segmentlen/2.0) for i in curpos]
    offset = [i for i in range(0, int(segmentlen))]
    index_start = pm.findpos(pm.comparison(start, 1))
    for i in index_start:
        start[i] = 1
    endpos = [i + (segmentlen - 1) for i in start]
    index = pm.findpos(pm.comparison(endpos, lenx, False))
    for i in index:
        endpos[i] = lenx
        start[i] = lenx + 1 - segmentlen

    win = windowing(segmentlen + 1, wintype)  # get our window
    frames = {}
    for i in range(0, numFrames):
        frames[i] = []
        for j in range(0, int(segmentlen)):
            # multiply the window index by the proper x index based on the
            # starting position and offset (segment is framed)
            frames[i].append(x[int(start[i]) + int(offset[j]) - 1] * win[j])
    return frames


def windowing(window_len: int, wt):
    """
    Similar implementation to the scipy windowing function, except this one provides
    more control over the output
    :param window_len: Length of the desired window
    :param wt: window type desired
    :return: window
    """
    nn = int(window_len) - 1
    n = [i for i in range(0, nn)]
    pn = [((2 * math.pi) * (float(i) / nn)) for i in range(0, nn)]
    if wt == "rect":
        w = [1] * int(window_len)
    elif wt == "tria":
        m = nn / 2
        w = [float(i) / m for i in range(0, m)]
        w = w + reversed(w[0:math.ceil(m) - 1])
    elif wt == "hann":
        w = [0.5 * (1. - math.cos(i)) for i in pn]
    elif wt == "hamm":
        w = [.54 - .46 * math.cos(i) for i in pn]
    else:
        w = [0] * int(window_len)
    return w


def get_log_spectrum(self, segment, fftlen, limit, logf, interp_logf):
    """
    Perform the FFT and return the log specturm
    :param segment:
    :param fftlen:
    :param limit:
    :param logf:
    :param interp_logf:
    :return:
    """
    spectra = np.fft.fft(segment, fftlen)
    spectra = spectra.tolist()  # convert the np array to a normal list
    amp = [abs(i) for i in spectra[0:(fftlen / 2) + 1]]
    amp = amp[1:int(limit) + 2]  # only keep values to the limit
    interp_amp = np.interp(interp_logf, logf, amp)
    # subtract the min value from each value in the list and convert the list
    interp_amp = [i - min(interp_amp.tolist()) for i in interp_amp.tolist()]
    return interp_amp


def wav_to_float(wav_file) -> float:
    """
    Python reads in the data as ints. Convert the ints into floating point numbers
    from the wav file, then take the sin of the float value
    :param wav_file:
    :return:
    """
    w = wave.open(wav_file)
    astr = w.readframes(w.getnframes())
    # convert binary chunks to short
    a = struct.unpack("%ih" % (w.getnframes() * w.getnchannels()), astr)
    a = [float(val) / pow(2, 15) for val in a]
    for i in range(0, len(a)):
        a[i] = round(math.sin(a[i]), 4)
    return w.getframerate(), a


def power_of_wave(y, fs, f0minmax=[50, 500], framelength=40, timestep=10, ceiling=1250):
    """
    Get the power of the wave using the Root Mean Squared function. Split the wave into blocks
    or frames and take the root mean squared value of each frame.
    :param y:
    :param fs:
    :param f0minmax:
    :param framelength:
    :param timestep:
    :param ceiling:
    :return:
    """
    minf0 = f0minmax[0]
    maxf0 = f0minmax[1]
    duration = framelength

    # pre processing input signal
    y = pm.dc_removal(y)
    y = pm.normalize(y)  # normalization
    total_len = len(y)

    # specify some algorithm specific thresholds
    interp_depth = 0.5  # for fft length

    # derived thresholds specific to the algorithm
    maxlogf = math.log((maxf0 / 2.), 2)
    minlogf = math.log((minf0 / 2.), 2)
    N = (math.floor(ceiling / minf0) - math.floor(ceiling / minf0) % 2) * 4

    # derive number of frames necessary to use
    segmentlen = round(duration * (float(fs) / 1000.))
    inc = round(float(timestep) * (float(fs) / float(1000)))
    nf = int(math.floor((total_len - segmentlen + inc) / inc))
    n = [i for i in range(1, nf + 1)]

    f0_time = [(i - 1) * float(timestep) + (duration/2) for i in n]

    # segmentation of speech
    curpos = [round((i / 1000) * fs) for i in f0_time]
    frames = to_frames(y, curpos, segmentlen, "hamm")

    rmsdata = []
    for i in range(0, len(frames)):
        rmsdata.append(pm.rms(frames[i]))
    return rmsdata


def get_note(freq):
    """
    Get the frequency of the middle C from the pitch table
    :param freq:
    :return:
    """
    # get the frequency of the middle C from the table
    c = int(Pitch.table[Pitch.middlepos][1])
    # calculate the offset from Middle C
    h = round(12.0 * math.log(float(freq) / float(c)) / math.log(2))
    # return the musical note and the column number from the table
    return Pitch.table[int(Pitch.middlepos + h)][0], Pitch.table[int(Pitch.middlepos + h)][2]


