# Biometrics

Facial recognition software systems have existed for years, however, systems based on the analysis of vocal characteristics remain in the adolescent stages. 
The ability to capture speech and determine a speaker’s emotion from the audio wave is, currently, being tested by the world’s leading universities. These 
universities, such as Massachusetts Institute of Technology (M.I.T) and Carnegie Mellon University (C.M.U), specialize in the fields of computer science, 
specifically artificial intelligence, and communication sciences. Recently, great progress has emerged in the study of emotion recognition through audio 
analysis, but the accuracy of such systems does not meet required standards.

The bulk of the project is located in [PDA](/pda) and [Pitch Math](/pitch_math). The project was originally created for research purposes, but I have since converted
the project to modules to be used by anyone that wants to continue this work. 

## Subdirectories ##

- [Analysis](/Analysis) - The results using several means of statistical analysis output in Microsoft EXCEL files. The original data was run through the algorithms in pda. 
- [Database](/database) - Module for my internal use only. This module can be modified or a new one can be written to wrap a database for storing information. The module is actually used to extract information from the database I used.
- [Features](/features) - External module that was also pulled from github called sigproc. The library was used to create sliding frames or sliding windows of data for analysis of sections of data.
- [PDA](/Pitch/pda) - The module contains the pitch determining algorithm(s) that I used for the analysis. 
- [Pitch](/Pitch/pitch) - Simple module with a lookup table for all pitches and the musical note associated with the pitch.
- [Pitch Math](/Pitch/pitch_math) - Utility module for PDA.
- [Research Paper](/Research_Paper) - Paper that details the conclusions that were reached through the statistical analysis.

## Module Installation

```console
pip3.4 install --user .
```

## Module Update
```console
pip3.4 install --user --upgrade .
```

## Required Modules ##
- [Features](/features)
- MySQLdb
- NumPy
- [PyAudio](https://pypi.org/project/PyAudio/#files)
- Struct
- Wave

